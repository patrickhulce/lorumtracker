import os
import os.path
import math
import random
from collections import deque

import Image
import ImageEnhance
import ImageStat
import ImageOps
import ImageDraw


CONTRAST_FACTOR = 5
THRESHOLD = 250
BRIGHTNESS_DATA = []
CONTRAST_DATA = []
CONTIGUOUS_BLACK_DATA = []


class ImageSlice:
    def __init__(self, source, x, y, w, h, label=None):
        self.source = source
        self.position = (x, y)
        self.box = (x, y, x + w, y + h)
        self.label = label
        self.slice = source.crop(self.box)
        self.size = (w, h)
        self._normalized = None

    def to_vector(self, pixels=400):
        vector = []
        scaled = ImageSlice.resize(self.normalized, total_px=400)
        pixels = scaled.load()
        w, h = scaled.size
        for x in range(w):
            for y in range(h):
                pixel = pixels[x, y]
                if len(vector) == pixels:
                    break
                vector.append(pixel / THRESHOLD)
        vector.append(self.label)
        return vector

    @property
    def normalized(self):
        if self._normalized is not None:
            return self._normalized
        self._normalized = ImageSlice.normalize(self.slice)
        return self._normalized

    @property
    def stats(self):
        image = self.normalized
        stat = ImageStat.Stat(image)
        b = stat.mean[0]
        c = stat.stddev[0]
        BRIGHTNESS_DATA.append(b)
        CONTRAST_DATA.append(c)
        return (b, c)

    @property
    def keep(self):
        stats = self.stats
        return stats[0] < 80 and stats[0] > 20

    @staticmethod
    def resize(image, total_px=None, max_size=None, width=None, height=None):
        w, h = image.size
        ratio = float(w) / h
        if total_px is not None:
            nh = int(math.sqrt(total_px / ratio))
            nw = int(ratio * nh)
            return image.resize((nw, nh))
        if max_size is not None:
            if ratio > 1:
                return image.resize((max_size, max_size / w))
            return image.resize((int(h * max_size), max_size))
        if width is not None and height is not None:
            return image.resize((width, height))

    @staticmethod
    def normalize(image):
        image = image.convert('L')
        contrast = ImageEnhance.Contrast(image)
        image = contrast.enhance(CONTRAST_FACTOR)
        stat = ImageStat.Stat(image)
        if stat.mean[0] > 100:
            image = ImageOps.invert(image)
        image = image.point(lambda p: p > THRESHOLD and 255)
        return image


class ImageSlicer:
    def __init__(self, source):
        self.image = source

    def slices(self, size, label, step=None):
        if step is None:
            step = size / 3
        w, h = self.image.size
        failed = {}
        x = 0
        while x < w - size:
            x_sz = x / step
            if x_sz not in failed:
                failed[x_sz] = []
            y = 0
            while y < h - size:
                y_sz = y / step
                if y_sz in failed[x_sz]:
                    y += step
                    continue
                s = ImageSlice(self.image, x, y, size, size, label)
                if s.keep:
                    yield s
                else:
                    failed[x_sz].append(y_sz)
                y += step
            x += step


class ImageComposite:
    def __init__(self, slices):
        self.slices = slices
        self.slice_size = slices[0].size

        self.source = slices[0].source
        self.size = self.source.size

        self._shaded = None
        self._composite = None
        self._used_pixels = {}

    @property
    def shaded(self):
        if self._shaded is not None:
            return self._shaded
        img = Image.new("RGB", self.size, "#000")
        draw = ImageDraw.Draw(img)
        for s in self.slices:
            draw.rectangle(s.box, fill="red")
        self._shaded = img
        return img

    @property
    def composite(self):
        if self._composite is not None:
            return self._composite
        img = Image.new("RGB", self.size, "#000")
        for s in self.slices:
            img.paste(s.normalized, s.position)
        self._composite = img
        return img

    def guess_columns(self):
        x_classifier = KMeans(4, lambda b: (b[0] + b[2]) / 2)
        img, boxes = self.guess_bounding_boxes()
        classified = x_classifier.classify(boxes)
        draw = ImageDraw.Draw(img)
        for c in classified[0]:
            draw.rectangle((c - 50, 0, c + 50, img.size[1]), outline="blue")
            draw.rectangle((c - 51, 1, c + 51, img.size[1] - 1), outline="blue")
            draw.rectangle((c - 49, 2, c + 49, img.size[1] - 2), outline="blue")

        y_classifier = KMeans(32, lambda b: b[3])
        y_classified = y_classifier.classify(boxes)
        for c in y_classified[0]:
            draw.line((0, c, img.size[0], c), width=5, fill="green")
        return img

    def guess_bounding_boxes(self):
        boxes = []
        img = self.composite.copy()
        draw = ImageDraw.Draw(img)
        raw_pixels = img.load()
        pixels = [[raw_pixels[x, y][0] > THRESHOLD for y in range(self.size[1])] for x in range(self.size[0])]
        used_pixels = {}
        print self.composite.size
        for x in range(self.size[0]):
            for y in range(self.size[1]):
                box = self._find_box(pixels, (x, y))
                if box is None:
                    continue
                w, h = (box[2] - box[0] + 1, box[3] - box[1] + 1)
                area = w * h
                desired_area = self.size[0] * self.size[1]
                if area < 30 or w > h * 2:
                    continue
                print box
                draw.rectangle(box, outline="red")
                boxes.append(box)
        return (img, boxes)


    def _find_box(self, pixels, position):
        used = self._used_pixels
        new_pixels = []
        q = deque([position])
        while len(q) != 0:
            x, y = q.popleft()
            if not pixels[x][y]:
                continue
            if x in used and y in used[x]:
                continue
            if x not in used:
                used[x] = {}
            new_pixels.append((x, y))
            used[x][y] = True
            adjacent_px = []
            for i in range(-1, 2):
                for j in range(-1, 2):
                    adjacent_px.append((x + i, y + j))
            for px in adjacent_px:
                if self._valid_pixel(px):
                    q.append(px)
        if len(new_pixels) == 0:
            return None
        min_x = min(new_pixels, key=lambda px: px[0])[0]
        min_y = min(new_pixels, key=lambda px: px[1])[1]
        max_x = max(new_pixels, key=lambda px: px[0])[0]
        max_y = max(new_pixels, key=lambda px: px[1])[1]
        return (min_x, min_y, max_x, max_y)

    def _valid_pixel(self, px):
        is_used = px[0] in self._used_pixels and px[1] in self._used_pixels[px[0]]
        in_image_x = px[0] < self.size[0] and px[0] >= 0
        in_image_y = px[1] < self.size[1] and px[1] >= 0
        return in_image_x and in_image_y and not is_used


class KMeans:
    def __init__(self, classes, obj_int_func):
        self.classes = range(classes)
        self.mapper = obj_int_func

    def classify(self, objects):
        centroids = self._randomize_centroids(objects)
        prev = []
        while prev != centroids:
            prev = centroids
            classified = self._classify(centroids, objects)
            lengths = [len(classified[l]) for l in classified]
            if min(lengths) == 0:
                centroids = self._randomize_centroids(objects)
            else:
                centroids = self._compute_centroids(classified)
        ret_val = []
        for c, objects in classified.iteritems():
            ret_val.extend([(c, obj) for obj in objects])
        return (centroids, ret_val)

    def _randomize_centroids(self, objects):
        centroids = []
        for _ in self.classes:
            i = random.randint(0, len(objects))
            centroids.append(self.mapper(objects[i]))
        return centroids


    def _classify(self, centroids, objects):
        classifications = {}
        for i in range(len(centroids)):
            classifications[i] = []
        for obj in objects:
            num = self.mapper(obj)
            distances = [abs(c - num) for c in centroids]
            c = distances.index(min(distances))
            classifications[c].append(obj)
        return classifications

    def _compute_centroids(self, classifications):
        new_centroids = []
        for _, objects in classifications.iteritems():
            values = [self.mapper(obj) for obj in objects]
            new_centroids.append(sum(values) / len(values))
        return new_centroids
