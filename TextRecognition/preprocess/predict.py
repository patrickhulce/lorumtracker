from numpy import dot, exp, transpose
import numpy as np
import convert
import Image
import os, os.path

CURRENT_PATH = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

THETA1_PATH = os.path.join(CURRENT_PATH,"data/vectors/id_theta1.csv")
THETA2_PATH = os.path.join(CURRENT_PATH,"data/vectors/id_theta2.csv")

IMAGE_PATH = os.path.join(CURRENT_PATH,"data/prediction_input/test_image.jpg")

X = []
Theta1 = []
Theta2 = []
mapping = {}

def sigmoid(array):
	denom = 1.0 + exp(-1.0 * array)
	return 1.0 / denom

def set_X_from_image():
	image = Image.open(IMAGE_PATH)
	slices = convert.slice_image(image,60)
	for i,s in enumerate(slices):
		new_slice = convert.process_slice(s)
		v = convert.to_400px_vector(new_slice)
		vec = [1]
		vec.extend(v)
		mapping[len(X)] = s
		X.append(vec)

def set_X_from_csv():
	with open("data.csv","r") as f:
		for line in f:
			values = line.split(",")
			mapping[len(X)] = values[0]
			xis = values[1:-1]
			xi = [1]
			xi.extend([int(x) for x in xis])
			X.append(xi)

def set_T1():
	with open(THETA1_PATH,"r") as f:
		for line in f:
			Theta1.append([float(x) for x in line.split(",")])


def set_T2():
	with open(THETA2_PATH,"r") as f:
		for line in f:
			Theta2.append([float(x) for x in line.split(",")])

def get_H1():
	z = dot(X,transpose(Theta1))
	return sigmoid(z)

def get_H2(H1):
	ones = np.ones((len(H1),1))
	H1 = np.append(ones,H1,1)
	z = dot(H1,transpose(Theta2))
	return sigmoid(z)

def get_predictions(H2):
	return [2 if row[1] > row[0] else 1 for row in H2]

def get_digits(preds):
	return [mapping[i] for i,p in enumerate(preds) if p == 2]

def compute_solution():
	pass

if __name__ == "__main__":
	print "Reading Xs"
	set_X_from_image()
	print "Reading thetas"
	set_T1()
	set_T2()
	print "Compute first layer"
	h1 = get_H1()
	print "Compute 2nd layer"
	h2 = get_H2(h1)
	print "Calcualte predictions"
	preds = get_predictions(h2)
	for i,img in mapping.iteritems():
		img.save("predictions/%d_predicted_%d.jpg" % (i,preds[i]))

	sorted_preds = sorted([row[1] for row in h2])
	results = get_digits(preds)
	for r in results:
		r.show()
		raw_input("Next...")