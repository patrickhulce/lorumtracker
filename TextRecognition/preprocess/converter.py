import Image
import convert
import random
import os, os.path, sys
import matplotlib.pyplot as plt
from convert import *

CURRENT_PATH = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

BORDER_SLICES_PATH = os.path.join(CURRENT_PATH,"data/column_output/border_slices/")

ID_SLICES_PATH = os.path.join(CURRENT_PATH,"data/column_output/slices/")
LINES_PATH = os.path.join(CURRENT_PATH,"data/lines_output/")
PHOTOS_PATH = os.path.join(CURRENT_PATH,"data/photos_input/")
NORMALIZED_SLICES_PATH = os.path.join(CURRENT_PATH,"data/normalized/")

VECTORS_PATH = os.path.join(CURRENT_PATH,"data/vectors/")

VECTOR_SUMS = []

COMPOSITE = None

def write_vectors_to_csv(vectors,filename):
	with open(VECTORS_PATH+filename,"w") as f:
		for v in vectors:
			vector_string = ",".join([str(x) for x in v]) + "\n"
			f.write(vector_string)

def ximages(path):
	for root, _, files in os.walk(path):
		for name in files:
			if os.path.splitext(name)[1] != ".jpg":
				continue
			full_path = os.path.join(root,name)
			image = Image.open(full_path)
			label = int(name.split("_")[0])
			yield (image,label)

def get_images(path):
	images = []
	for root, _, files in os.walk(path):
		for name in files:
			if os.path.splitext(name)[1] != ".jpg":
				continue
			full_path = os.path.join(root,name)
			image = Image.open(full_path)
			label = int(name.split("_")[0])
			images.append((image,label))
	return images

def contiguous_black(vector,number):
	max_row = 0
	in_a_row = 0
	for x in vector:
		if x == 0:
			in_a_row += 1
		else:
			max_row = max(max_row,in_a_row)
			in_a_row = 0
	return in_a_row >= number

def convert_slices_to_vectors(slices):
	vectors = []
	for s in slices:
		file_name = "%d_normalized_%d.jpg" % (s.label,random.randint(0,100000))
		normalized_s = s.normalized
		normalized_s.save(NORMALIZED_SLICES_PATH + file_name)
		print file_name

		vector = s.to_vector()
		VECTOR_SUMS.append(sum(vector))
		vectors.append(vector)
	return vectors

def convert_images_to_vectors(images):
	global COMPOSITE
	vectors = []
	for image,label in images:
		size = image.size[0] / 25
		slicer = ImageSlicer(image)
		slices = [s for s in slicer.slices(60,label=1,step=20)]
		vectors.extend(convert_slices_to_vectors(slices))
		COMPOSITE = ImageComposite(slices)
		COMPOSITE.guess_bounding_boxes()[0].show()
	return vectors

def convert_to_id_vector(do_images,do_ids,do_lines):
	vectors = []
	if do_images:
		full_images = ximages(PHOTOS_PATH)
		vectors.extend(convert_images_to_vectors(full_images))
		os.system("rm %sid_data_images.csv" % VECTORS_PATH)
		write_vectors_to_csv(vectors,"id_data_images.csv")
	if do_ids:
		id_slices = ximages(BORDER_SLICES_PATH)
		vectors.extend(convert_slices_to_vectors(id_slices))
		os.system("rm %sid_data_images.csv" % VECTORS_PATH)
		write_vectors_to_csv(vectors,"id_data_digits.csv")
	if do_lines:
		line_slices = ximages(LINES_PATH)
		vectors.extend(convert_slices_to_vectors(line_slices))
		os.system("rm %sid_data_images.csv" % VECTORS_PATH)
		write_vectors_to_csv(vectors,"id_data_lines.csv")
	os.system("rm %sid_data.csv" % VECTORS_PATH)
	write_vectors_to_csv(vectors,"id_data.csv")

if __name__ == "__main__":
	os.system("rm -fR %s" % NORMALIZED_SLICES_PATH)
	os.system("mkdir " + NORMALIZED_SLICES_PATH)
	print "0 : Convert all identification data"
	print "1 : Convert id images only"
	print "2 : Convert id data only"
	print "3 : Convert id lines only"
	response = raw_input("Your choice: ")
	if response == "0":
		convert_to_id_vector(True,True,True)
	elif response == "1":
		convert_to_id_vector(True,False,False)
	elif response == "2":
		convert_to_id_vector(False,True,False)
	elif response == "3":
		convert_to_id_vector(False,False,True)