import generate
import random
import os, os.path


FONT_PATH = "/usr/share/fonts/truetype"

CURRENT_PATH = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
DIGIT_OUTPUT_PATH =  os.path.join(CURRENT_PATH,"data/digit_output/")
SCORE_OUTPUT_PATH =  os.path.join(CURRENT_PATH,"data/score_output/")
COLUMN_OUTPUT_PATH =  os.path.join(CURRENT_PATH,"data/column_output/")
LINES_OUTPUT_PATH = os.path.join(CURRENT_PATH,"data/lines_output/")

OMITTED_FONTS = ["cmex10.ttf","cmsy10.ttf","esint10.ttf","msam10.ttf","msbm10.ttf","wasy10.ttf","webdings.ttf","Webdings.ttf"]
BACKGROUND = "#FFF"
FILL = "#000"

TEST_FONT = generate.Font("FreeSans.ttf","/usr/share/fonts/truetype/freefont/FreeSans.ttf")

def get_fonts():
	"""Returns list of (fontname,path) objects from all TTF files in the FONT_PATH"""
	fonts = []
	for root, _, files in os.walk(FONT_PATH):
		for name in files:
			path = os.path.join(root,name)
			if os.path.splitext(path)[1] == ".ttf" and name not in OMITTED_FONTS:
				fonts.append(generate.Font(name,path))
	return fonts	


def test_font_size():
	i50 = generate.generate_digit(4,TEST_FONT,FILL,50,BACKGROUND)
	i30 = generate.generate_digit(1,TEST_FONT,FILL,30,BACKGROUND)
	i20 = generate.generate_digit(9,TEST_FONT,FILL,20,BACKGROUND)
	i50.show()
	i30.show()
	i20.show()

def generate_sample_digits():
	os.system("rm %s*" % DIGIT_OUTPUT_PATH)
	fonts = get_fonts()
	for font in fonts:
		for i in range(10):
			image = generate.generate_digit(i,font,FILL,30,BACKGROUND)
			image.save(DIGIT_OUTPUT_PATH + "%d_%s.jpg" % (i,font.name))

def generate_sample_scores():
	os.system("rm %s*" % SCORE_OUTPUT_PATH)
	fonts = get_fonts()
	for i,font in enumerate(fonts):
		n = random.randint(1,150)
		image = generate.generate_score(n,fonts,FILL,30,BACKGROUND)
		image.save(SCORE_OUTPUT_PATH + "score_%d.jpg" % i)

def generate_sample_columns():
	os.system("rm -fR %s*" % COLUMN_OUTPUT_PATH)
	os.system("mkdir %sslices" % COLUMN_OUTPUT_PATH)
	os.system("mkdir %sborder_slices" % COLUMN_OUTPUT_PATH)
	fonts = get_fonts()
	for i in range(30):
		image,offsets = generate.generate_column(40,fonts,FILL,30,BACKGROUND)
		base_name = "column%d" % i
		image.save(COLUMN_OUTPUT_PATH + base_name + ".jpg")
		big_image = generate.pad_image(image,10,"#FFF")
		big_offsets = [x + 10 for x in offsets]
		generate_column_slices(base_name + "big",big_image,big_offsets,70)
		generate_column_slices(base_name + "regular",image,offsets,50)
		generate_column_slices("tight",image,offsets,35)


def generate_column_slices(name,image,offsets,window):
	all_slices_regular = generate.generate_column_all_slices(image,10,window)
	good_slices = generate.generate_column_good_slices(image,offsets,30)
	bad_slices = generate.generate_column_bad_slices(image,offsets,30)
	for j, slce in enumerate(all_slices_regular):
		slce.save(COLUMN_OUTPUT_PATH + "slices/2_%s_%d.jpg" % (name, j))
	for j, slce in enumerate(good_slices):
		slce.save(COLUMN_OUTPUT_PATH + "border_slices/2_%s_%d.jpg" % (name, j))
	for j, slce in enumerate(bad_slices):
			slce.save(COLUMN_OUTPUT_PATH + "border_slices/1_%s_%d.jpg" % (name, j))

def generate_random_lines():
	os.system("rm %s*" % LINES_OUTPUT_PATH)
	for i in xrange(10000):
		number = random.randint(2,8)
		image = generate.generate_random_line(number,FILL,20,BACKGROUND)
		image.save(LINES_OUTPUT_PATH + "1_%d.jpg" % i)

if __name__ == "__main__":
	print "0 : Test Font Sizes"
	print "1 : Generate Digits"
	print "2 : Generate Scores"
	print "3 : Generate Columns"
	print "4 : Generate Lines"
	response = raw_input("Your choice: ")
	if response == "0":
		test_font_size()
	elif response == "1":
		generate_sample_digits()
	elif response == "2":
		generate_sample_scores()
	elif response == "3":
		generate_sample_columns()
	elif response == "4":
		generate_random_lines()
