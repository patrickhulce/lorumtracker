import Image
import ImageDraw
import ImageFont
import os, os.path
import random

FONT_PATH = "/usr/share/fonts/truetype"
SAVE_PATH = "output/"

OMITTED_FONTS = ["cmex10.ttf","cmsy10.ttf","esint10.ttf","msam10.ttf","msbm10.ttf","wasy10.ttf","webdings.ttf","Webdings.ttf"]
BACKGROUNDS = ["#fff","#ddd"]
FILLS = ["#000","#333","#666"]


def get_fonts():
	"""Returns list of (fontname,ImageFont) objects from all TTF files in the FONT_PATH"""
	fonts = []
	for root, _, files in os.walk(FONT_PATH):
		for name in files:
			path = os.path.join(root,name)
			if os.path.splitext(path)[1] == ".ttf" and name not in OMITTED_FONTS:
				fonts.append((name,ImageFont.truetype(path,16)))
	return fonts		

def generate_digits_for_font(font):
	"""Saves image files for each digit in all fills for a given font"""
	for i in range(10):
		for fill in FILLS:
			for bg in BACKGROUNDS:
				save_digit_to_image(i,font,fill,bg)

def save_digit_to_image(digit, font, fill, background):
	"""Saves the specified digit to a file using font, fill, and background settings"""
	offset = (random.randint(0,12),random.randint(-2,3))
	image = Image.new("RGBA", (20,20))
	draw = ImageDraw.Draw(image)
	draw.text(offset,str(digit),font=font[1],fill=fill)
	save_path = SAVE_PATH + "%d_%s_%s_%d.jpg" % (digit, font[0], fill, random.randint(0,1000))
	print save_path
	image.save(save_path)
	return image


def test_image_creation():
	path = "/usr/share/fonts/truetype/freefont/FreeSans.ttf"
	font = ImageFont.truetype(path,16)
	image = save_digit_to_image(7,font,"#999","#fff")
	image.show()

if __name__ == "__main__":
	fonts = get_fonts()
	for font in fonts:
		generate_digits_for_font(font)