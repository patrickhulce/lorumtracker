import Image
import ImageDraw
import ImageFont
import os, os.path
import random

class Font:
	def __init__(self,name,path):
		self.name = name[0:-4]
		self.path = path

	def get_image_font(self,size):
		return ImageFont.truetype(self.path,size)

def new_canvas(width,height,background=None):
	canvas_sz = (width,height)
	if background is None:
		canvas = Image.new("RGBA",canvas_sz)
	else:
		canvas = Image.new("RGB", canvas_sz, background)
	return canvas


def generate_digit(digit,font,fill,size,background=None):
	digit = str(digit)
	d_font = font.get_image_font(size)
	#Setup fake canvas to get the desired size
	sizer = ImageDraw.Draw(Image.new("RGB",(1,1)))
	d_w, d_h = sizer.textsize(digit,font=d_font)
	#Setup the actual canvas slightly
	canvas = new_canvas(d_w,int(d_h*1),background)
	draw = ImageDraw.Draw(canvas)
	offset = (0,int(-0.15*d_h))
	draw.text(offset,digit,font=d_font,fill=fill)
	return canvas

def generate_score(score,fonts,fill,size,background=None):
	images = []
	gap_max = int(size / 8.0)
	score = str(score)
	for i in range(len(score)):
		digit = score[i]
		font = fonts[random.randint(0,len(fonts)-1)]
		image = generate_digit(digit,font,fill,size)
		gap = random.randint(-1*gap_max,int(gap_max*.5))
		images.append((gap,image))
	image_width = sum([i[1].size[0] for i in images])
	width = image_width + sum([i[0] for i in images])
	height = max([i[1].size[1] for i in images])
	canvas = new_canvas(width,height,background)
	running_offset = 0
	for i, (gap,image) in enumerate(images):
		offset = (running_offset + gap,0)
		canvas.paste(image,offset,image)
		running_offset = offset[0] + image.size[0]
	return canvas

def decorate_score(score):
	padding = random.randint(0,5)
	image = new_canvas(score.size[0],score.size[1])
	draw = ImageDraw.Draw(image)
	bb = [(0,0),image.size]
	options = {'outline' : "#000",'fill' : None}
	if padding > 2:
		draw.ellipse(bb,**options)
	else:
		draw.rectangle(bb,**options)
	return image
	
def generate_column(length,fonts,fill,size,background=None):
	total = 0
	scores = []
	gap_max = int(size / 4.0)
	for i in range(length):
		points = random.randint(1,10)
		total += points
		score = generate_score(str(total),fonts,fill,size)
		gap = -1 * random.randint(gap_max,2*gap_max)
		scores.append((gap,score))
	scores[0] = (0,scores[0][1])
	image_height = sum([s[1].size[1] for s in scores])
	height = image_height + sum([s[0] for s in scores])
	width = max([s[1].size[0] for s in scores])
	canvas = new_canvas(width,height,background)
	running_offset = 0
	offsets = []
	for gap,image in scores:
		running_offset += gap
		print "Image is %dpx tall starting at %d" % (image.size[1], running_offset)
		offsets.append(running_offset)
		width_offset = int(width / 2.0 - image.size[0] / 2.0)
		offset = (width_offset,running_offset)
		canvas.paste(image,offset,image)
		if random.randint(0,10) > 8:
			dec_offset = (offset[0] - gap_max / 2,offset[1] - gap_max / 2)
			decoration = decorate_score(image)
			canvas.paste(decoration,dec_offset,decoration)
		running_offset = offset[1] + image.size[1]
	return (canvas,offsets)

def generate_random_line(number,fill,size,background=None):
	image = new_canvas(size,size,background)
	draw = ImageDraw.Draw(image)
	vert = random.randint(0,10) > 5
	val = random.randint(0,size)
	off1 = random.randint(-2,2)
	off2 = random.randint(-2,2)
	point1 = (0,val+off1)
	point2 = (size,val+off2)
	if vert:
		point1 = (val+off1,0)
		point2 = (val+off2,size)
	draw.line([point1,point2],fill=fill,width=2)
	return image

def generate_column_good_slices(image,offsets,size):
	slices = []
	half_size = size / 2
	center_x = image.size[0] / 2
	left = center_x - size / 2
	right = center_x + size / 2
	for i,start in enumerate(offsets[:-1]):
		center_y = (start + offsets[i+1]) / 2
		top = center_y - size / 2
		bottom = center_y + size / 2
		slices.append(image.crop((left,top,right,bottom)))
	return slices


def generate_column_bad_slices(image,offsets,size):
	slices = []
	w,h = image.size
	step = size / 6
	for center_y in offsets:
		for i in range((w - size)/step):
			left = step * i
			right = left + size
			top = center_y - size / 2
			bottom = center_y + size / 2
			slices.append(image.crop((left,top,right,bottom)))
	return slices

def generate_column_all_slices(image,step,window_size):
	slices = []
	iters_y = (image.size[1] - window_size) / step + 1
	for i in range(iters_y):
		top = i * step
		bottom = top + window_size
		iters_x = (image.size[0] - window_size) / step + 1
		for j in range(iters_x):
			left = j * step
			right = left + window_size
			slices.append(image.crop((left,top,right,bottom)))
	return slices

def pad_image(image,padding,background=None):
	width = image.size[0] + padding * 2
	height = image.size[1] + padding * 2
	new_image = new_canvas(width,height,background)
	new_image.paste(image,(padding,padding))
	return new_image


