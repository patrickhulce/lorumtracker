import Image
import ImageDraw

class ImageComposite:
    def __init__(self, slices):
        self.slices = slices
        self.slice_size = slices[0].size

        self.source = slices[0].source
        self.size = self.source.size

    def shaded(self,color="red",background="#000"):
        img = Image.new("RGB", self.size, background)
        draw = ImageDraw.Draw(img)
        for s in self.slices:
            draw.rectangle(s.box, fill=color)
        return img

    def composite(self,background="#000"):
        img = Image.new("RGB", self.size, background)
        for s in self.slices:
            img.paste(s.normalized, s.position)
        return img