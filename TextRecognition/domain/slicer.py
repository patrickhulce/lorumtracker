from slice import ImageSlice

class ImageSlicer:
    def __init__(self, image):
        self.image = image
        self.size = image.size

    def region_slices(self, slice_w, slice_h, label=None, step=None, region=None):
        if region is None:
            w, h = self.size
            region = (0, 0, w, h)
        if step is None:
            step = (slice_w + slice_h) / 6
        x = region[0]
        while x + slice_w < region[2]:
            y = region[1]
            while y + slice_h < region[3]:
                s = ImageSlice(self.image, x, y, slice_w, slice_h, label=label)
                yield s
                y += step
            x += step

    def list_slices(self, positions, slice_w, slice_h, label=None):
        for pos in positions:
            s = ImageSlice(self.image, pos[0], pos[1], slice_w, slice_h, label=label)
            yield s