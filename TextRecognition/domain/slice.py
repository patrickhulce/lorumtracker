import ImageStat
import manipulate


class ImageSlice:
    def __init__(self, source, x, y, w, h, label=None, contrast=5, threshold=250):
        self.position = (x, y)
        self.size = (w, h)
        self.box = (x, y, x + w, y + h)

        self.source = source
        self.image = source.crop(self.box)

        self.label = 0 if label is None else label
        self.contrast = contrast
        self.threshold = threshold

        self._normalized = None

    def vector(self, length):
        return manipulate.truth_vector(self._normalized, length, self.label)

    def padded(self,padding,background,size=None):
        return manipulate.pad(self.image,padding,background,size)

    @property
    def normalized(self):
        if self._normalized is not None:
            return self._normalized
        self._normalized = manipulate.normalize(self.image, self.contrast, self.threshold)
        return self._normalized

    @property
    def brightness(self):
        image = self.normalized
        stat = ImageStat.Stat(image)
        return stat.mean[0]

    @property
    def keep(self):
        return 20 < self.brightness < 80