import math
import Image
import ImageEnhance
import ImageStat
import ImageOps


def pad(image, padding, background, size=None):
    w, h = image.size
    if size is None:
        size = (w + padding * 2, h + padding * 2)
    available_size = (size[0] - padding * 2, size[1] - padding * 2)
    padded = Image.new("RGB", size, background)
    resized = resize(image, max_dimensions=available_size)
    padded.paste(image, resized)
    return padded


def resize(image, target_size=None, total_px=None, max_size=None, max_dimensions=None):
    w, h = image.size
    ratio = float(w) / h
    size = (w, h)
    if target_size is not None:
        size = target_size
    elif total_px is not None:
        nh = int(math.sqrt(total_px / ratio))
        nw = int(ratio * nh)
        size = (nw, nh)
    elif max_size is not None:
        if ratio > 1:
            size = (max_size, int(max_size / ratio))
        else:
            size = (int(ratio * max_size), max_size)
    elif max_dimensions is not None:
        max_ratio = float(max_dimensions[0]) / max_dimensions[1]
        if max_ratio > ratio:
            size = (int(max_dimensions[1] * ratio), max_dimensions[1])
        else:
            size = (max_dimensions[0], int(max_dimensions[0] / ratio))
    return image.resize(size)


def normalize(image, contrast_factor, threshold_value):
    image = image.convert('L')
    contrast = ImageEnhance.Contrast(image)
    image = contrast.enhance(contrast_factor)
    stat = ImageStat.Stat(image)
    if stat.mean[0] > 100:
        image = ImageOps.invert(image)
    image = image.point(lambda p: p > threshold_value and 255)
    return image


def truth_vector(image, length, label):
    vector = []
    scaled = resize(image, total_px=length)
    length = scaled.load()
    w, h = scaled.size
    for x in range(w):
        for y in range(h):
            pixel = length[x, y]
            if len(vector) == length:
                break
            vector.append(pixel[0] > 125)
    if len(vector) != length:
        vector.extend([False for _ in range(length - len(vector))])
    vector.append(label)
    return vector
