input_layer_size  = 400;  % 20x20 Input Images of Digits
hidden_layer_size = 100;  % 100 hidden units
num_labels = 10;          % 10 labels, from 1 to 10 

load('data.mat')
sel = randperm(size(X, 1));
sel = sel(1:100);

displayData(X(sel, :));
pause;
fprintf("Initializing weights...\n");

initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);

% Unroll parameters
initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

options = optimset('MaxIter', 100);

%  You should also try different values of lambda
lambda = 0;

% Create "short hand" for the cost function to be minimized
costFunction = @(p) nnCostFunction(p, ...
                                   input_layer_size, ..
.                                   hidden_layer_size, ...
                                   num_labels, X, y, lambda);


fprintf("Performing optimization...\n");

[nn_params, cost] = fmincg(costFunction, initial_nn_params, options);

fprintf("Done!\n");

Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1))

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1))

displayData(Theta1(:, 2:end));
pause;

save digitsTheta.mat Theta1 Theta2

pred = predict(Theta1, Theta2, X);

fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);
pause;