
fprintf("Reading data...\n");

data = csvread('../data/vectors/id_data.csv');
X = data(:,1:size(data,2)-1);
y = data(:,size(data,2))

save id_data.mat X y

fprintf("Saving data...\n");