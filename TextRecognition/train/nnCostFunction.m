function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));


X_ones = [ones(m,1) X];
z2 = X_ones * Theta1';
A2 = [ones(m,1) sigmoid(z2)];
z3 = A2 * Theta2';
A3 = sigmoid(z3);
y_matrix = zeros(m,num_labels);
for i = 1:m,
    k = y(i);
    y_matrix(i,k) = 1;
endfor;

costs = -y_matrix  .* log(A3) - (1 - y_matrix) .* log(1 - A3);
regular = lambda  * (sum(sum(Theta1(:,2:end) .^ 2)) + sum(sum(Theta2(:,2:end) .^ 2))) / (2 * m);
J = sum(sum(costs)) / m + regular;


D1 = zeros(size(Theta1));
D2 = zeros(size(Theta2));
for i = 1:m,
    a3_i = A3(i,:)';
    a2_i = A2(i,:)';
    a1_i = X_ones(i,:)';
    y_i = y_matrix(i,:)';
    delta3_i = a3_i - y_i;
    delta2_i = Theta2' * delta3_i .* sigmoidGradient([1; z2(i,:)']);
    D1 = D1 + (delta2_i(2:end,:) * a1_i');
    D2 = D2 + (delta3_i * a2_i');
endfor;

Theta1_grad = D1 / m;
Theta2_grad = D2 / m;

regular_theta_1 = Theta1;
regular_theta_1(:,1) = zeros(size(Theta1,1),1);
Theta1_grad = Theta1_grad + lambda / m .* regular_theta_1;

regular_theta_2 = Theta2;
regular_theta_2(:,1) = zeros(size(Theta2,1),1);
Theta2_grad = Theta2_grad + lambda / m .* regular_theta_2;

% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
