<html xmlns="http://www.w3.org/1999/html">
<head>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<title>LorumTracker</title>
<link rel="stylesheet" href="extras.css" />
<link rel="stylesheet"
	href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script
	src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
</head>
<body>
	<script type="text/javascript">
var app = {
    game : {},
    ui : {},
    pages : {},
    statistics : {},
    settings : {
        instructions : false,
        confirmation : true
    }
};

	//@TODO Check for one 0 on last two tricks


function RefreshUnfinishedGames() {
    $.post('games.php?action=recent', {timestamp: new Date().getTime()}, function (raw_data) {
        var data = $.parseJSON(raw_data);
        if (data.msg != "success") {
            alert("Something went wrong.");
        }
        else {
        	$(".unfinishedGame").remove();
            for (var i = data.games.length - 1; i >= 0; i--) {
                $("<li class='unfinishedGame'><a data-transition='slide' href='javascript: app.pages.loadGame(" + data.games[i].id + ");'>" + data.games[i].name + "</a></li>").insertAfter(".recentGames");
            }
            $('#main-nav').listview('refresh');
        }
    });
}

$(document).ready(function() {
    if(window.location.hash.length > 1 && window.location.hash != "#add-player") {
        window.location = "index.php";
    }
    window.onscroll = function() {
       if(window.location.hash == "#view-game" && window.scrollY > 90) {
            $(".floatingPlayers").show();
       }
        else {
           $(".floatingPlayers").hide();
       }
    };
    $('#toggleinstructions').change(function() {
        app.settings.instructions = ($(this).val() == "yes");
    });
    $('#toggleconfirmation').change(function() {
        app.settings.confirmation = ($(this).val() == "yes");
    });
    RefreshUnfinishedGames();
    setInterval(RefreshUnfinishedGames, 120000);
});
</script>
<script type="text/javascript" src="game.js"></script>
<script type="text/javascript" src="pages.js"></script>
	<!-- Home -->
	<div data-role="page" id="home-page">
		<div data-role="header">
			<h3>LorumTracker</h3>
		</div>
		<div data-role="content">
			<ul id="main-nav" data-role="listview" data-divider-theme=""
				data-inset="true">
				<li class='recentGames' data-role="list-divider" role="heading">
					Recent Unfinished Games</li>
				<li data-role="list-divider" role="heading">Games</li>
				<li><a href="javascript: app.pages.loadPlayersForNewGame();"
					data-transition="slide"> New Game </a>
				</li>
				<li><a href="javascript: app.pages.loadGameHistory();" data-transition="slide">
						Game History </a>
				</li>
				<li data-role="list-divider" role="heading">Extras</li>
				<li><a href="javascript: app.pages.loadPlayersForViewing();"
					data-transition="slide"> View Players </a>
				</li>
				<li><a href="javascript: app.pages.loadLeaderboards();"
					data-transition="slide"> Leaderboards </a></li>
				<li><a href="#settings" data-transition="slide"> Settings </a>
				</li>
			</ul>
		</div>
	</div>

	<!-- New game -->
	<div data-role="page" id="new-game">
		<div data-role="header">
			<a data-role="button" href="#home-page" data-icon="home"
				data-iconpos="left" class="ui-btn-left"> Home </a>

			<h3>New Game</h3>
		</div>
		<div data-role="content">
			<form id="newGameForm" action="#" method="POST">
				<div data-role="fieldcontain">
					<label for="player1"> Player 1: </label> <select id="player1"
						name="player1" class="playerNames">
						<option value="none">Please pick one...</option>
					</select>
				</div>
				<div data-role="fieldcontain">
					<label for="player2"> Player 2: </label> <select id="player2"
						name="player2" class="playerNames">
						<option value="none">Please pick one...</option>
					</select>
				</div>
				<div data-role="fieldcontain">
					<label for="player3"> Player 3: </label> <select id="player3"
						name="player3" class="playerNames">
						<option value="none">Please pick one...</option>
					</select>
				</div>
				<div data-role="fieldcontain">
					<label for="player4"> Player 4: </label> <select id="player4"
						name="player4" class="playerNames">
						<option value="none">Please pick one...</option>
					</select>
				</div>
				<div data-role="fieldcontain">
					<label>
				        <input id="is-ranked" type="checkbox" name="ranked" value="1">Ranked Game
				    </label>
				</div>
			</form>
			<a data-role="button" data-transition="slide"
				href="javascript: app.game.createGame();"> Play! </a> <a data-role="button"
				data-transition="slide" href="#add-player"> Don't See Your Name? </a>
		</div>
	</div>

	<!-- Game View -->
	<div data-role="page" id="view-game">
		<div data-role="header">
			<a data-role="button" href="#home-page" data-icon="home"
				data-iconpos="left" class="ui-btn-left"> Home </a>

			<h3 class="gameTitle">Play Lorum</h3>


			<a data-role="button" href="#" data-icon="info" data-iconpos="notext"
				class="ui-btn-right instructionsLink"> </a> 
				<a id="bulkEnterLink"
				data-role="button"
				href="javascript: app.pages.setupEnterGame();"
				data-icon="star" data-iconpos="notext"> </a>
				<a id="refreshLink"
				data-role="button"
				href="javascript: app.pages.loadGame(app.game.id);"
				data-icon="refresh" data-iconpos="notext"> </a>
		</div>
		<div data-role="content">
			<div class='players'>
				<div id="player1Name" class="player player1Name">Player 1</div>
				<div id="player2Name" class="player player2Name">Player 2</div>
				<div id="player3Name" class="player player3Name">Player 3</div>
				<div id="player4Name" class="player player4Name">Player 4</div>
			</div>
			<div class="floatingPlayers">
				<div class="player player1Name">Player 1</div>
				<div class="player player2Name">Player 2</div>
				<div class="player player3Name">Player 3</div>
				<div class="player player4Name">Player 4</div>
			</div>
			<div class="scores"></div>

			<a id="enterScore" data-role="button" data-transition="slide"
				href="javascript: app.game.displayScoreForms();"> Enter Score </a>

			<div id="scoringForms" style="display: none">
				<h3 class="gameTitle"></h3>
				<div id="normalForm">
					<div class="standard">
						<div data-role="fieldcontain">
							<fieldset data-role="controlgroup">
								<label class="player1Name" for="player1Score"> Player 1: </label>
								<input id="player1Score" type="range" name="player1Score"
									value="0" min="0" max="10" data-highlight="false">
							</fieldset>
						</div>
						<div data-role="fieldcontain">
							<fieldset data-role="controlgroup">
								<label class="player2Name" for="player2Score"> Player 2: </label>
								<input id="player2Score" type="range" name="player2Score"
									value="0" min="0" max="10" data-highlight="false">
							</fieldset>
						</div>
						<div data-role="fieldcontain">
							<fieldset data-role="controlgroup">
								<label class="player3Name" for="player3Score"> Player 3: </label>
								<input id="player3Score" type="range" name="player3Score"
									value="0" min="0" max="10" data-highlight="false">
							</fieldset>
						</div>
						<div data-role="fieldcontain">
							<fieldset data-role="controlgroup">
								<label class="player4Name" for="player4Score"> Player 4: </label>
								<input id="player4Score" type="range" name="player4Score"
									value="0" min="0" max="10" data-highlight="false">
							</fieldset>
						</div>
						<a data-role="button" href='javascript: app.game.updateScore(0);'>Record
							Score</a> <a data-role="button"
							href="javascript: $('.shootTheMoon').show(); $('.standard').hide();">Shooting
							the Moon?</a>
					</div>
					<div class="shootTheMoon" style="display: none;">
						<form id="moonshooterForm">
							<div data-role="fieldcontain">
								<fieldset data-role="controlgroup" data-type="horizontal">
									<legend> Who shot the moon? </legend>
									<input id="player1moonshooter" name="moonshooter" value="0"
										type="radio"> <label class="player1NameRadio"
										for="player1moonshooter"> Player 1 </label> <input
										id="player2moonshooter" name="moonshooter" value="1"
										type="radio"> <label class="player2NameRadio"
										for="player2moonshooter"> Player 2 </label> <input
										id="player3moonshooter" name="moonshooter" value="2"
										type="radio"> <label class="player3NameRadio"
										for="player3moonshooter"> Player 3 </label> <input
										id="player4moonshooter" name="moonshooter" value="3"
										type="radio"> <label class="player4NameRadio"
										for="player4moonshooter"> Player 4 </label>
								</fieldset>
							</div>
						</form>
						<a data-role="button" href='javascript: app.game.updateScore(1);'>Record
							Score</a> <a data-role="button"
							href="javascript: $('.shootTheMoon').hide(); $('.standard').show();">Just
							Kidding</a>
					</div>
				</div>
				<div id="tenPointForm">
					<form id="bomedForm">
						<div data-role="fieldcontain">
							<fieldset data-role="controlgroup" data-type="horizontal">
								<legend> Who got bombed? </legend>
								<input id="player1bombed" name="bombed" value="0" type="radio">
								<label class="player1NameRadio" for="player1bombed"> Player 1 </label>
								<input id="player2bombed" name="bombed" value="1" type="radio">
								<label class="player2NameRadio" for="player2bombed"> Player 2 </label>
								<input id="player3bombed" name="bombed" value="2" type="radio">
								<label class="player3NameRadio" for="player3bombed"> Player 3 </label>
								<input id="player4bombed" name="bombed" value="3" type="radio">
								<label class="player4NameRadio" for="player4bombed"> Player 4 </label>
							</fieldset>
						</div>
					</form>
					<a data-role="button" href='javascript: app.game.updateScore(2);'>Record
						Score</a>
				</div>
			</div>
		</div>
	</div>

	<!-- player info -->
	<div data-role="page" id="view-player">
		<div data-role="header">
			<a data-role="button" href="javascript: app.pages.loadPlayersForViewing();"
				data-icon="back" data-iconpos="left" class="ui-btn-left"> Back </a>

			<h3>View Player</h3>
		</div>
		<div data-role="content">
			<h3 class="playerInfoName">Player Name</h3>
			<div class="playerInfoMain"></div>
			<div class="playerInfoDetails"></div>
		</div>
	</div>

	<!-- player leaderboards -->
	<div data-role="page" id="leaderboards">
		<div data-role="header">
			<a data-role="button" href="#home-page" data-icon="home"
				data-iconpos="left" class="ui-btn-left"> Home </a>

			<h3>Leaderboards</h3>
		</div>
		<div data-role="content">
			<div class="playerInfoLeaderboards"></div>
		</div>
	</div>


	<!-- Game history -->
	<div data-role="page" id="list-games">
		<div data-role="header">
			<a data-role="button" href="#home-page" data-icon="home"
				data-iconpos="left" class="ui-btn-left"> Home </a>

			<h3>Game History</h3>
		</div>
		<div data-role="content">
			<ul id="gameHistoryList" data-role="listview" data-inset="true">
			</ul>
		</div>
	</div>

	<!-- View Players -->
	<div data-role="page" id="list-players">
		<div data-role="header">
			<a data-role="button" href="#home-page" data-icon="home"
				data-iconpos="left" class="ui-btn-left"> Home </a>

			<h3>View Players</h3>
		</div>
		<div data-role="content">
			<ul id="playersList" data-role="listview" data-inset="true">
			</ul>
		</div>
	</div>

	<!-- Enter Game -->
	<div data-role="page" id="enter-game">
		<div data-role="header">
			<a data-role="button" href="#home-page" data-icon="home"
				data-iconpos="left" class="ui-btn-left"> Home </a>

			<h3>Enter Game</h3>
		</div>
		<div data-role="content">
			<div class='players'>
				<div class="player player1Name">Player 1</div>
				<div class="player player2Name">Player 2</div>
				<div class="player player3Name">Player 3</div>
				<div class="player player4Name">Player 4</div>
			</div>
			<div class="floatingPlayers">
				<div class="player player1Name">Player 1</div>
				<div class="player player2Name">Player 2</div>
				<div class="player player3Name">Player 3</div>
				<div class="player player4Name">Player 4</div>
			</div>
			<form id="bulk-enter-form">
						
					</form>
					<a data-role="button" href='javascript: app.game.bulkEnter();'>Record
						Scores</a>
		</div>
	</div>

	<div id="add-player" data-role="dialog">
		<div data-role="header">
			<h1>Add a Player</h1>
		</div>
		<div data-role="content">
			<p>Add a player below so that they can participate in games!</p>

			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup">
					<label for="newPlayerName"> </label> <input name="name"
						id="newPlayerName" placeholder="Player name..." value=""
						type="text">
				</fieldset>
			</div>

			<a data-role="button" href='javascript: app.game.addPlayer();'>Add</a>
		</div>
	</div>

	<div id="settings" data-role="dialog">
		<div data-role="header">
			<h1>Settings</h1>
		</div>
		<div data-role="content">
			<p>Show instructions before each game?</p>

			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup">
					<label for="toggleinstructions"> </label> 
					<select name="toggleinstructions"
						id="toggleinstructions" data-role="slider">
						<option value="no" selected>No</option>
						<option value="yes">Yes</option>
					</select>
				</fieldset>
			</div>
			<p>Show confirmation before submitting the scores?</p>

			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup">
					<label for="toggleconfirmation"> </label> 
					<select name="toggleconfirmation"
						id="toggleconfirmation" data-role="slider">
						<option value="no">No</option>
						<option value="yes" selected>Yes</option>
					</select>
				</fieldset>
			</div>
		</div>
	</div>

	<div style="display: none">
		<a id="dummyLink" href="#" data-transition="fade"></a>
	</div>

	<div id="heartsInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Standard trick-taking game. Each heart you take is a point. Take
				them all and you receive 0 points while every other player receives
				10! No playing hearts on the first trick.</p>
			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>

	<div id="uppersInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Standard trick-taking game. Each felso (upper/far apart) you take
				gives you points, but take them all and every other player receives
				10 points! Hearts: 4pts, bells: 3pts, leaves: 2pts, acorns: 1pt. No
				uppers may be played on the first trick unless it is your only card
				of the suit being lead.</p>
			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>


	<div id="eachInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Standard trick-taking game. Each trick you take gives you a point,
				but take them all and every other player receives 10 points.</p>
			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>


	<div id="lastInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Standard trick-taking game. Whoever takes the last trick of the
				hand receives 10 points. A redeal may be called if a player has 7 or
				more face cards.</p>

			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>


	<div id="kingInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Standard trick-taking game with a twist! Everyone holds their
				cards facing toward the other players (don't look at your cards!),
				and whoever takes the king of hearts gets 10 points.</p>
			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>


	<div id="babyInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Standard trick-taking game. Everyone gangs up on the leader in
				this race to find the makk also (lower of acorns). The person across
				from the taker of the baby blue gets 10 points.</p>
			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>


	<div id="quartetInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Play cards by making sets of four. The person who has the highest
				card of the set takes the lead. Sets need not be a complete four.
				Possible scenarios to maintain the lead: leader has a complete set
				of 4 (or partial set with highest card having no immediate
				superior), leader has a partial set of 4 including the highest card
				(the missing cards are taken from the other players), leader has a
				card with no immediate superior. If none of these scenarios match,
				the leader must play a partial set of 4 and allow the remaining
				cards to be filled in from other players and the highest card takes
				the lead. The game ends when one player runs out of cards. If the
				leader goes out, only players filling the spaces in between the
				played cards may play (no cards may be played on top). The number of
				remaining cards in each players' hands are the points received.</p>
			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>


	<div id="7upInstructions" data-role="dialog">
		<div data-role="header">
			<h1 class="gameTitle">Game</h1>
		</div>
		<div data-role="content">
			<p>Leader chooses a start card. Players then take turns playing
				either the next card of that suit or one of the start cards in
				another suit. If no possible moves can be made, the player passes.
				You may not pass if a play is possible. The game ends when a player
				runs out of cards. The number of cards remaining in the players'
				hands are the number of points received. If a player has 3 of the
				card immediately below the start card of 4 of the card two below, a
				redeal can be called.</p>

			<a data-role="button" data-rel="back" href="#">Continue</a>
		</div>
	</div>

</body>
</html>
