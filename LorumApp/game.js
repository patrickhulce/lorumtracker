(function(game, $, undefined) {
    game.id = 0;
    game.round = 0;
    game.hand = 0;
    game.players = [0, 0, 0, 0];
    game.totalScores = [0, 0, 0, 0];
    game.roundScores = [
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ],
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ],
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ],
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ]
    ];

    game.setHandState = function() {
        var instrDiv;
        switch (game.hand) {
            case 0:
                if (game.round == 4) {
                    $(".gameTitle").text("Game Finished");
                } else {
                    $(".gameTitle").html("Piros Fog&aacute;s");
                    instrDiv = "#heartsInstructions";
                }
                break;
            case 1:
                $(".gameTitle").html("Fels&oacute; Fog&aacute;s");
                instrDiv = "#uppersInstructions";
                break;
            case 2:
                $(".gameTitle").html("&Uuml;t&eacute;s Fog&aacute;s");
                instrDiv = "#eachInstructions";
                break;
            case 3:
                $(".gameTitle").html("Utols&oacute; Fog&aacute;s");
                instrDiv = "#lastInstructions";
                break;
            case 4:
                $(".gameTitle").html("Piros Kir&aacute;ly");
                instrDiv = "#kingInstructions";
                break;
            case 5:
                $(".gameTitle").html("Makk Als&oacute;");
                instrDiv = "#babyInstructions";
                break;
            case 6:
                $(".gameTitle").html("Kvartett");
                instrDiv = "#quartetInstructions";
                break;
            case 7:
                $(".gameTitle").html("Lerak&oacute;s");
                instrDiv = "#7upInstructions";
                break;
            default:
                alert("What happened?");
                break;
        }
        if (app.settings.instructions) {
            app.pages.goTo(instrDiv);
        }
        $(".instructionsLink").attr("href", instrDiv);
    }

    game.setGameState = function(scoreArr) {
                    game.round = 0;
                    game.hand = 0;
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 8; j++) {
                game.roundScores[i][j][0] = parseInt(scoreArr[i][j][0]);
                game.roundScores[i][j][1] = parseInt(scoreArr[i][j][1]);
                game.roundScores[i][j][2] = parseInt(scoreArr[i][j][2]);
                game.roundScores[i][j][3] = parseInt(scoreArr[i][j][3]);
                game.round = i;
                game.hand = j;
                if (scoreArr[i][j][0] == 0 && scoreArr[i][j][1] == 0 && scoreArr[i][j][2] == 0 && scoreArr[i][j][3] == 0) {
                    i = 10;
                    j = 10;
                } else if (i == 3 && j == 7) {
                    game.round = 4;
                    game.hand = 7;
                }
            }
        }
        if(game.round==0 && game.hand ==0) {
            $("#bulkEnterLink").show();
        }
        else {
            $("#bulkEnterLink").hide();
        }
        $(".scores").html('');
        game.totalScores = [0, 0, 0, 0];
        for (var i = 0; i <= game.round; i++) {
            var roundContent = "";
            for (var j = 0; j < 8; j++) {
                if (i == game.round && j >= game.hand || i == 4) {

                } else {
                    classT = ["<span class='bombed'>","</span>"];
                    classF = ["",""];
                    class1 = game.roundScores[i][j][0] == 10 ? classT : classF;
                    class2 = game.roundScores[i][j][1] == 10 ? classT : classF;
                    class3 = game.roundScores[i][j][2] == 10 ? classT : classF;
                    class4 = game.roundScores[i][j][3] == 10 ? classT : classF;
                    game.totalScores[0] += game.roundScores[i][j][0];
                    game.totalScores[1] += game.roundScores[i][j][1];
                    game.totalScores[2] += game.roundScores[i][j][2];
                    game.totalScores[3] += game.roundScores[i][j][3];
                    roundContent += "<div class='hand'>" +
                        "<div class='cell'>" + class1[0] + game.totalScores[0] + class1[1] + "</div>" +
                        "<div class='cell'>" + class2[0] +game.totalScores[1] + class2[1] + "</div>" +
                        "<div class='cell'>" + class3[0] +game.totalScores[2] + class3[1] + "</div>" +
                        "<div class='cell'>" + class4[0] +game.totalScores[3] + class4[1] + "</div>" +
                        "</div>";
                }
            }
            $(".scores").append("<div class='round'>" + roundContent + "</div>");
        }
    }

    game.updateScore = function(type) {
        var points = [0, 0, 0, 0];
        switch (type) {
            case 0:
                points[0] = parseInt($('#player1Score').val());
                points[1] = parseInt($('#player2Score').val());
                points[2] = parseInt($('#player3Score').val());
                points[3] = parseInt($('#player4Score').val());
                break;
            case 1:
                var moonshot = parseInt($('input:radio[name=moonshooter]:checked').val());
                points = [10, 10, 10, 10];
                points[moonshot] = 0;
                break;
            case 2:
                var bombed = parseInt($('input:radio[name=bombed]:checked').val());
                points[bombed] = 10;
                break;
        }
        var pointSum = points[0] + points[1] + points[2] + points[3];
        var postData = {};
        postData["game_id"] = game.id;
        postData["round"] = game.round;
        postData["hand"] = game.hand;
        postData["player1"] = points[0];
        postData["player2"] = points[1];
        postData["player3"] = points[2];
        postData["player4"] = points[3];
        postData["timestamp"] = new Date().getTime();
        if ((game.hand == 0 || game.hand == 2) && (pointSum != 8 && pointSum != 30)) {
            alert("These points don't seem to add up. Can you double check?");
        } else if (game.hand == 1 && pointSum != 10 && pointSum != 30) {
            alert("These points don't seem to add up. Can you double check?");
        } else {
            if(app.settings.confirmation == true) {
                var confirmMsg = "You are submitting for round " + (game.round+1) + 
                ", game " + $('.gameTitle').first().text() + ".\n" + " Are these scores correct?\n" +
                $('.player1Name').first().text() + " : " + points[0] + "\n" +
                $('.player2Name').first().text() + " : " + points[1] + "\n" +
                $('.player3Name').first().text() + " : " + points[2] + "\n" +
                $('.player4Name').first().text() + " : " + points[3];
                var result = confirm(confirmMsg);
                if(result == false) {
                    return;
                }
            }
            $.post('games.php?action=update', postData, function(raw_data) {
                var data = $.parseJSON(raw_data);
                if (data.msg != "success") {
                    alert("An error occurred!");
                } else {
                    classT = ["<span class='bombed'>","</span>"];
                    classF = ["",""];
                    class1 = points[0] == 10 ? classT : classF;
                    class2 = points[1] == 10 ? classT : classF;
                    class3 = points[2] == 10 ? classT : classF;
                    class4 = points[3] == 10 ? classT : classF;
                    game.totalScores[0] += points[0];
                    game.totalScores[1] += points[1];
                    game.totalScores[2] += points[2];
                    game.totalScores[3] += points[3];
                    var newHand = "<div class='hand'>" +
                        "<div class='cell'>" + class1[0] + game.totalScores[0] + class1[1] + "</div>" +
                        "<div class='cell'>" + class2[0] +game.totalScores[1] + class2[1] + "</div>" +
                        "<div class='cell'>" + class3[0] +game.totalScores[2] + class3[1] + "</div>" +
                        "<div class='cell'>" + class4[0] +game.totalScores[3] + class4[1] + "</div>" +
                        "</div>";
                    $(".round").last().append(newHand);
                    $("#scoringForms").hide();
                    if (game.round == 3 && game.hand == 7) {
                        $('#enterScore').hide();
                    } else {
                        $('#enterScore').show();
                        if (game.hand == 7) {
                            $(".scores").append("<div class='round'></div>");
                            game.hand = -1;
                            game.round++;
                        }
                        game.hand++;
                        game.setHandState();
                    }
                }
            });
        }
    }

    game.addPlayer = function() {
        $.post('players.php?action=add', {
            name: $("#newPlayerName").val()
        }, function(raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("Something went wrong. Did you enter a name?");
            } else {
                app.pages.loadPlayersForNewGame();
            }
        });
    }

    game.createGame = function() {
        $.post('games.php?action=new', $('#newGameForm').serialize() + "&timestamp=" + new Date().getTime(), function(raw_data) {
            console.log("here");
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("Something went wrong. Did you pick 4 players?");
            } else {
                app.pages.loadGame(data.id);
            }
        });
    }

    game.displayScoreForms = function() {
        $('#enterScore').hide();
        $("#scoringForms > div").hide();
        $("#scoringForms").show();
        if (game.hand == 0 || game.hand == 1 || game.hand == 2 || game.hand == 6 || game.hand == 7) {
            $("#normalForm").show();
            $("input[type=number]").val(0).slider('refresh');
            if (game.hand == 6 || game.hand == 7) {
                $(".standard").show();
                $(".shootTheMoon").hide();
            }
        } else if (game.hand == 3 || game.hand == 4 || game.hand == 5) {
            $("#tenPointForm").show();
        }
        window.scrollTo(0, $('#scoringForms').offset().top);
    }

    game.bulkEnter = function() {
        if(game.round != 0 || game.hand != 0) {
            return;
        }
        $.post('games.php?action=bulkenter',$("#bulk-enter-form").serialize(),function(resp) {
            var data = $.parseJSON(resp);
            alert(data.msg);
            app.pages.loadGame(game.id);
        });
    }

}(window.app.game, jQuery));