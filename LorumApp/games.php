<?php
require 'connect.php';
error_reporting(E_ERROR);
header("Cache-Control: no-cache;");
date_default_timezone_set("America/Chicago");
function getPlayerName($id) {
    $rs = mysql_query("SELECT name FROM players WHERE id = " . $id) or die(mysql_error());
    $ret = mysql_fetch_assoc($rs) or die("No such player");
    return $ret['name'];
}

function getPlayerInfo($id) {
    $rs = mysql_query("SELECT name FROM players WHERE id = " . $id) or die(mysql_error());
    $ret = mysql_fetch_assoc($rs) or die("No such player");
    return $ret;
}

function computeElo($pos, $n, $ptsoff, $elos) {
    $change = 0;
    $mine = $elos[$pos];
    foreach($ptsoff as $i => $pts) {
        if($pts != 0) {
            $other = $elos[$i];
            $won = $pts / abs($pts + .00000000001);
            $z = log(abs($other - $mine))*1.43 - 8.57;
            $skill = 400 / (1 + pow(2.718,-1 * $z)) * ($other > $mine ? 1 : -1);
            $for_i = ($won * 400 + $skill) / 3 * (1 + abs($pts) / 10) / 2;
            $change += $for_i;
        }
    }
    return $mine + $change / ($n + 1);
}

function computeElos($scores,$players) {
    $totals = array(0,0,0,0);
    foreach($scores as $round) {
        foreach($round as $hand) {
            foreach($totals as $i => $x) {
                $totals[$i] += $hand[$i];
            }
        }
    }
    $elos = array();
    $n = array();
    $ptsoff = array();
    foreach($players as $i => $player) {
        $elos[] = $player['elo'];
        $n[] = min($player['maturity'],20);
        $diff = array();
        foreach($totals as $total) {
            $diff[] = $total - $totals[$i];
        }
        $ptsoff[] = $diff;
    }
    return array(
        computeElo(0,$n[0],$ptsoff[0],$elos),
        computeElo(1,$n[1],$ptsoff[1],$elos),
        computeElo(2,$n[2],$ptsoff[2],$elos),
        computeElo(3,$n[3],$ptsoff[3],$elos)
        );
}

//New game
if ($_GET['action'] == "new" && is_numeric($_POST['player1']) && is_numeric($_POST['player2']) && is_numeric($_POST['player3']) && is_numeric($_POST['player4'])) {
    $scores = array(
        array(array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0)),
        array(array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0)),
        array(array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0)),
        array(array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0))
    );
    $score_txt = json_encode($scores);
    $score_sql = addslashes($score_txt);
    $time = time();
    $ranked = (is_numeric($_POST['ranked']) ? 1 : 0);
    mysql_query(sprintf("INSERT INTO games (scores,timestamp,player1,player2,player3,player4,`ranked`) VALUES ('%s',%d,%d,%d,%d,%d,%d)", $score_sql, $time, $_POST['player1'], $_POST['player2'], $_POST['player3'], $_POST['player4'], $ranked)) or die(mysql_error());
    $rs = mysql_query("SELECT * FROM games WHERE timestamp = " . $time) or die(mysql_error());
    $info = mysql_fetch_assoc($rs);
    die(json_encode(array("msg" => "success", "id" => $info['id'])));

}
else if($_GET['action'] == "view" && is_numeric($_POST['game_id'])) {
    $rs = mysql_query("SELECT * FROM games WHERE id = " . $_POST['game_id']) or die(mysql_error());
    $info = mysql_fetch_assoc($rs);
    if($info) {
        $players = array();
        $players[] = getPlayerName($info['player1']);
        $players[] = getPlayerName($info['player2']);
        $players[] = getPlayerName($info['player3']);
        $players[] = getPlayerName($info['player4']);
        $info["scores"] = json_decode($info['scores']);
        die(json_encode(array("msg" => "success", "info" => $info,"players" => $players)));
    }
    die(json_encode(array("msg" => "error", "post" => $_POST)));

}
else if($_GET['action'] == "update" && is_numeric($_POST['round'])
    && is_numeric($_POST['hand']) && is_numeric($_POST['player1'])
    && is_numeric($_POST['player2']) && is_numeric($_POST['player3'])
    && is_numeric($_POST['player4']) && is_numeric($_POST['game_id'])) {
    $round = $_POST['round'];
    $hand = $_POST['hand'];
    $rs = mysql_query("SELECT * FROM games WHERE id = " . $_POST['game_id']) or die(mysql_error());
    $info = mysql_fetch_assoc($rs);
    if($info) {
        $scores = json_decode($info['scores']);
        if($scores[$round][$hand] == array(0,0,0,0)) {
            $additional = " round = " . $round . ", hand = " . $hand . ",";
        }
        else {
            $additional = "";
            die(json_encode(array("error"=>"Not up to date.")));
        }
        if($round == 3 && $hand == 7 && $info['ranked'] == 1) {
            //Update Elos
            $playerinfo = array(
                getPlayerInfo($info['player1']),
                getPlayerInfo($info['player2']),
                getPlayerInfo($info['player3']),
                getPlayerInfo($info['player4'])
                );
            $elos = computeElos($scores,$playerinfo);
            foreach($playerinfo as $i => $player) {
                mysql_query(sprintf("UPDATE players SET elo = %d, maturity = %d WHERE id = %d",$elos[$i],$player['maturity'] + 1,$player['id'])) or die(mysql_error());
            }
        }
        $scores[$round][$hand] = array((int) $_POST['player1'],(int) $_POST['player2'],(int) $_POST['player3'],(int) $_POST['player4']);
        $q = "UPDATE games SET$additional scores = '" . addslashes(json_encode($scores)) . "' WHERE id = " . $_POST['game_id'];
        mysql_query($q) or die(json_encode(array("error"=>mysql_error(),"query"=>$q)));
        die(json_encode(array("msg" => "success", "query" => $q)));
    }
    die(json_encode(array("msg" => "error", "post" => $_POST)));
}
else if($_GET['action'] == "list") {
    $rs = mysql_query("SELECT * FROM games ORDER BY `timestamp` DESC") or die(mysql_error());
    $games = array();
    while($game = mysql_fetch_assoc($rs)) {
        $game['name'] = (( $game['round'] * 8 + $game['hand'] + 1) / 32  * 100) . "% done " . date("M j",$game['timestamp']) . " at " . date("g:ia",$game['timestamp']);
        $games[] = $game;
    }
    die(json_encode(array("msg" => "success", "games" => $games)));
}
else if($_GET['action'] == "recent") {
    $rs = mysql_query("SELECT * FROM games WHERE round <> 3 OR (round = 3 AND hand <> 7) ORDER BY `timestamp` DESC LIMIT 3") or die(mysql_error());
    $games = array();
    while($game = mysql_fetch_assoc($rs)) {
        $game['name'] = (( $game['round'] * 8 + $game['hand'] + 1) / 32  * 100) . "% with " .
            getPlayerName($game['player1']) . ", " .
            getPlayerName($game['player2']) . ", " .
            getPlayerName($game['player3']) . ", " .
            getPlayerName($game['player4']) .
            " on " .date("M j",$game['timestamp']);
        $games[] = $game;
    }
    die(json_encode(array("msg" => "success", "games" => $games)));
}
else if($_GET['action'] == "elo") {
    mysql_query("UPDATE players SET elo = 1000, maturity = 1");
    $rs = mysql_query("SELECT * FROM games WHERE round = 3 AND hand = 7 AND ranked = 1 ORDER BY `timestamp` ASC") or die(mysql_error());
    $players = array();
    while($game = mysql_fetch_assoc($rs)) {
        $game_players = array();
        for($i=1;$i<=4;$i++) {
            $pname = "player$i";
            if(!isset($players[$game[$pname]])) {
                $players[$game[$pname]] = array("elo" => 1000, "maturity" => 1);
            }
            $game_players[] = $players[$game[$pname]];
        }
        $elos = computeElos(json_decode($game['scores']),$game_players);
        for($i=1;$i<=4;$i++) {
            $pname = "player$i";
            $pid = $game[$pname];
            echo "<b>Updating player $pid to elo " . $elos[$i-1] . " </b><br>";
            $players[$pid] = array("elo" => $elos[$i-1],"maturity" => $players[$pid]['maturity'] + 1);
        }
    }
    foreach($players as $pid => $player) {
        mysql_query(sprintf("UPDATE players SET elo = %d, maturity = %d WHERE id = %d",$player['elo'],$player['maturity'],$pid)) or die(mysql_error());
    }
}
else if ($_GET['action'] == "bulkenter" && is_numeric($_POST['game_id'])) {
    $scores = array();
    $round = 0;
    $hand = 0;
    $prev_scores = array(0,0,0,0,0,0);
    for($i=0;$i<4;$i++) {
        $scores[$i] = array();
        for($j=0;$j<8;$j++) {
            $scores[$i][$j] = array();
            for($k=1;$k<5;$k++) {
                $var_name = sprintf("score_%d_%d_%d",$i,$j,$k);
                $score = (int)$_POST[$var_name];
                $delta_score = $score - $prev_scores[$k-1];
                if($delta_score >= 0) {
                    $scores[$i][$j][] = $delta_score;
                    $prev_scores[$k-1] = $score;
                }
                else {
                    $scores[$i][$j][] = 0;
                }
            }
            if(array_sum($prev_scores) > 0) {
                $round = $i;
                $hand = $j;
            }
        }
    }
    $score_txt = json_encode($scores);
    $score_sql = addslashes($score_txt);
    $query = sprintf("UPDATE games SET scores = '%s', round = %d, hand = %d WHERE id = %d", $score_sql, $round,$hand, $_POST['game_id']);
    mysql_query($query) or die(mysql_error());
    die(json_encode(array("msg" => "Game successfully entered!","query"=>$query)));
}
else {
    die(json_encode(array("msg" => "error", "post" => $_POST)));
}