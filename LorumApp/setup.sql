CREATE TABLE games (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    round int(11),
    hand int(11),
    scores text,
    timestamp bigint(20),
    player1 int(11),
    player2 int(11),
    player3 int(11),
    player4 int(11),
    ranked tinyint(4)
);
CREATE TABLE players (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(128),
    elo int(11),
    maturity int(11)
);