(function(pages, $, undefined) {

    var goTo = function(page_id) {
        $('#dummyLink').attr("href", page_id);
        $('#dummyLink').click();
    }

    pages.goTo = goTo;

    pages.loadPlayer = function(p_id) {
        $.post('players.php?action=info', {
            player_id: p_id,
            timestamp: new Date().getTime()
        }, function(raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("Something went wrong.");
            } else {
                $(".playerInfoName").text(data.info.name);
                $(".playerInfoMain").html("");
                for (var i = 0; i < 3; i++) {
                    $(".playerInfoMain").append('<div class="playerInfoStat">' +
                        '<div class="title">' + data.info.statistics.main[i][0] + '</div>' +
                        '<div class="stat">' + data.info.statistics.main[i][1] + '</div>' +
                        '</div>');
                }
                $(".playerInfoDetails").html("");
                for (var i = 0; i < data.info.statistics.details.length; i++) {
                    $(".playerInfoDetails").append('<div class="playerInfoStat">' +
                        '<div class="title">' + data.info.statistics.details[i][0] + '</div>' +
                        '<div class="stat">' + data.info.statistics.details[i][1] + '</div>' +
                        '</div>');
                }
                goTo("#view-player");
            }
        });
    }

    pages.loadLeaderboards = function() {
        $.post('players.php?action=leaderboard', {
            timestamp: new Date().getTime()
        }, function(raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("Something went wrong.");
            } else {
                var leaderData = "";
                for (var i = 0; i < data.leaders.length; i++) {
                    leaderData += "<li><a class='name' href='javascript: app.pages.loadPlayer(" + data.leaders[i][1] + 
                        ");'>" + data.leaders[i][0] + "</a><span class='elo'>" + data.leaders[i][2] + "</span></li>";
                }
                $(".playerInfoLeaderboards").html("<h3>Top Players</h3><ol class='top-players'>"+leaderData+"</ol>");
                for (var i = 0; i < data.info.length; i++) {
                    $(".playerInfoLeaderboards").append('<div class="playerInfoStat">' +
                        '<div class="title">' + data.info[i][0] + '</div>' +
                        '<a class="name" href="javascript: app.pages.loadPlayer(' + data.info[i][2] + ');">' + data.info[i][1] + '</a>' +
                        '<div class="stat">' + data.info[i][3] + data.info[i][4] + '</div>' +
                        '</div>');
                }
                goTo("#leaderboards");
            }
        });
    }

    pages.loadPlayersForViewing = function() {
        $.post('players.php?action=list', {
            timestamp: new Date().getTime()
        }, function(raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("Something went wrong.");
            } else {
                currentPlayers = data.players;
                $("#playersList").html('');
                for (var i = 0; i < currentPlayers.length; i++) {
                    $("#playersList").append("<li><a data-transition='slide' href='javascript: app.pages.loadPlayer(" + currentPlayers[i].id + ");'>" + currentPlayers[i].name + "</a></li>");
                }
                goTo("#list-players");
                $('#playersList').listview('refresh');
            }
        });
    }

    pages.loadPlayersForNewGame = function() {
        $.post('players.php?action=list', {
            timestamp: new Date().getTime()
        }, function(raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("Something went wrong.");
            } else {
                currentPlayers = data.players;
                $(".playerNames").html("<option selected='selected' value='none'>Please pick one...</option>");
                for (var i = 0; i < currentPlayers.length; i++) {
                    $(".playerNames").append("<option value='" + currentPlayers[i].id + "'>" + currentPlayers[i].name + "</option>");
                }
                goTo("#new-game");
                $("#new-game select").selectmenu('refresh', true);
                $("#is-ranked").checkboxradio('refresh');
            }
        });
    }

    pages.loadGameHistory = function() {
        $.post('games.php?action=list', {
            timestamp: new Date().getTime()
        }, function(raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("Something went wrong.");
            } else {
                $("#gameHistoryList").html("");
                for (var i = 0; i < data.games.length; i++) {
                    $("#gameHistoryList").append("<li><a data-transition='slide' href='javascript: app.pages.loadGame(" + data.games[i].id + ");'>" + data.games[i].name + "</a></li>");
                }
                goTo("#list-games");
                $('#gameHistoryList').listview('refresh');
            }
        });
    }

    pages.loadGame = function(the_game_id) {
        var instructions = app.settings.instructions;
        if (typeof instructions != "boolean") {
            instructions = false;
        }
        app.game.id = the_game_id;
        $.post('games.php?action=view', {
            game_id: app.game.id,
            timestamp: new Date().getTime()
        }, function(raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.msg != "success") {
                alert("An error occurred!");
            } else {
                app.game.setGameState(data.info.scores);
                if (app.game.round == 3 && app.game.hand == 7) {
                    $('#enterScore').hide();
                } else {
                    $('#enterScore').css('display', 'block');
                }
                app.game.players[0] = data.info.player1;
                app.game.players[1] = data.info.player2;
                app.game.players[2] = data.info.player3;
                app.game.players[3] = data.info.player4;
                goTo("#view-game");
                $('.player1Name').text(data.players[0]);
                $('.player2Name').text(data.players[1]);
                $('.player3Name').text(data.players[2]);
                $('.player4Name').text(data.players[3]);
                $('.player1NameRadio').html('<span class="ui-btn-inner"><span class="ui-btn-text">' + data.players[0] + '</span></span>');
                $('.player2NameRadio').html('<span class="ui-btn-inner"><span class="ui-btn-text">' + data.players[1] + '</span></span>');
                $('.player3NameRadio').html('<span class="ui-btn-inner"><span class="ui-btn-text">' + data.players[2] + '</span></span>');
                $('.player4NameRadio').html('<span class="ui-btn-inner"><span class="ui-btn-text">' + data.players[3] + '</span></span>');
                $("#scoringForms").hide();
                app.game.setHandState();
            }
        });
    }

    pages.setupEnterGame = function() {
        $("#bulk-enter-form").html('');
        $("#bulk-enter-form").append("<input type='hidden' name='game_id' value='"+app.game.id+"' />");
        for(var r=1;r<5;r++) {
            var divContent = "";
            for(var h=1;h<9;h++) {
                var handStr = (r-1).toString() + "_" + (h-1).toString();
                divContent += "<fieldset data-role='controlgroup' data-type='horizontal'>"+
                                "<legend>Hand "+h.toString()+"</legend>"+
                                "<input type='number' value='0' name='score_"+handStr+"_1'/>"+
                                "<input type='number' value='0' name='score_"+handStr+"_2'/>"+
                                "<input type='number' value='0' name='score_"+handStr+"_3'/>"+
                                "<input type='number' value='0' name='score_"+handStr+"_4'/>"+
                                "<div class='clearer'></div>"+
                            "</fieldset>";
            }
            $("#bulk-enter-form").append('<div data-role="fieldcontain">'+divContent+'</div><div class="round-end"></div>');


        }
        $('#enter-game').trigger('create');
        pages.goTo("#enter-game");
    };

}(window.app.pages, jQuery));