(function(ui, $, undefined) {
    ui.addPlayerStat = function(stat_info) {
        $(".playerInfoMain").append('<div class="playerInfoStat">' +
            '<div class="title">' + stat_info[0] + '</div>' +
            '<div class="stat">' + stat_info[1] + '</div>' +
            '</div>');
    }

    ui.addLeaderboardStat = function(stat_info) {
        $(".playerInfoLeaderboards").append('<div class="playerInfoStat">' +
            '<div class="title">' + stat_info[0] + '</div>' +
            '<a class="name" href="javascript: app.pages.loadPlayer(' + stat_info[2] + ');">' + stat_info[1] + '</a>' +
            '<div class="stat">' + stat_info[3] + stat_info[4] + '</div>' +
            '</div>');
    }
}(window.app.ui, jQuery));