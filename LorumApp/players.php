<?php
require 'connect.php';
error_reporting(E_ERROR);
date_default_timezone_set("America/Chicago");

function formatPercent($percent) {
	return round($percent * 10000) / 100 . "%";
}

function formatAverage($avg) {
	return round($avg * 100) / 100;
}

function getPlayerName($id) {
	$rs = mysql_query("SELECT name FROM players WHERE id = " . $id) or die(mysql_error());
	$ret = mysql_fetch_assoc($rs) or die("No such player");
	return $ret['name'];
}

function getPlayerStats($id) {

}

function loadAllGames($p_id) {
	$games = array_merge(loadGamesFrom($p_id,1),loadGamesFrom($p_id,2),loadGamesFrom($p_id,3),loadGamesFrom($p_id,4));
	return $games;
}

function loadGamesFrom($p_id,$number) {
	$ret = array();
	$games_rs = mysql_query("SELECT * FROM games WHERE player$number = $p_id") or die(mysql_error());
	$a = 0;
	$game = mysql_fetch_assoc($games_rs);
	while($game != false && $a<100) {
		$a++;
		//Fix the scores
		$old_scores = json_decode($game['scores']);
		$sum = array(0,0,0,0);
		for($i=0;$i<4;$i++) {
			for($j=0;$j<8;$j++) {
				//Add for the sum
				$sum[0] +=  $old_scores[$i][$j][0];
				$sum[1] +=  $old_scores[$i][$j][1];
				$sum[2] +=  $old_scores[$i][$j][2];
				$sum[3] +=  $old_scores[$i][$j][3];
			}
		}
		$game['scores'] = $old_scores;
		$game['position'] = $number;
		$game['lead'] = $number == 1;
		$game['finished'] = $game['round'] == 3 && $game['hand'] == 7;
		$game['total'] = $sum[$number-1];
		sort($sum);
		if($sum[0] == $game['total']) {
			$game['rank'] = 1;
		}
		elseif($sum[1] == $game['total']) {
			$game['rank'] = 2;
		}
		elseif($sum[2] == $game['total']) {
			$game['rank'] = 3;
		}
		else {
			$game['rank'] = 4;
		}
		$ret[] = $game;
		$game = mysql_fetch_assoc($games_rs);
	}
	return $ret;
}

if($_GET['action'] == "add" && strlen($_POST['name']) > 1) {
	mysql_query(sprintf("INSERT INTO players (`name`) VALUES ('%s')",addslashes($_POST['name']))) or die(mysql_error());
	die(json_encode(array("msg" => "success")));
}
elseif($_GET['action'] == "list") {
    $players_rs = mysql_query("SELECT * FROM players ORDER BY name ASC") or die(mysql_error());
    $players = array();
    while($row = mysql_fetch_assoc($players_rs)) {
        $players[] = $row;
    }
    die(json_encode(array("msg" => "success","players" => $players)));
}
elseif($_GET['action'] == "info" && is_numeric($_POST['player_id'])) {
	$rs = mysql_query("SELECT * FROM players WHERE id = " . $_POST['player_id']) or die(mysql_error());
	$info = mysql_fetch_assoc($rs);
	if($info) {
		$games = loadAllGames($info['id']);
		$n = count($games);
		$game_stats = array();
		for($i=0;$i<8;$i++) {
			$game_stats[] = array("total" => 0, "attempts" => 0.000000001);
		}
		$best = 300;
		$worst = 0;
		$round_scores = 0;
		$rounds = 0;
		$moons = 0;
		$moon_opts = 0;
		foreach($games as $game) {
			if($game['finished'] === true) {
				$rank += $game['rank'];
				$score += $game['total'];
				$completed += 1;
				$best = min($best,$game['total']);
				$worst = max($worst,$game['total']);
			}
			foreach($game['scores'] as $round) {
				$round_total = 0;
				foreach($round as $i => $hand) {
					if($hand != array(0,0,0,0)) {
						$hs = $hand[$game['position']-1];
						$game_stats[$i]['total'] += $hs;
						$game_stats[$i]['attempts'] += 1;
						$round_total += $hs;
						$moons += (array_sum($hand) == 30 && $hs == 0) ? 1 : 0;
						$moon_opts += ($i==0||$i==1||$i==2) ? 1 : 0;
						if($i == 7) {
							$rounds++;
							$round_scores += $round_total;
						}
					}
				}
			}
		}
		$avg_rank = round($rank / $completed);
		switch($avg_rank) {
			case 1:
				$avg_rank .= "st";
				break;
			case 2:
				$avg_rank .= "nd";
				break;
			case 3:
				$avg_rank .= "rd";
				break;
			case 4:
				$avg_rank .= "th";
				break;
			default:
				$avg_rank = "N/A";
		}
		$info['statistics'] = array(
			'main' => array(
				array("Games Played",$n),
				array("Player Rating", ($info['elo'] == 1000 ? "N/A" : round($info['elo']))),
				array("Average Score/Game",formatAverage($score / $completed))
				),
			'details' => array(
				array("Games Completed",$completed),
				array("Completion %",formatPercent($completed / count($games))),
				array("Average Ranking",$avg_rank),
				array("Best Game",$best),
				array("Worst Game",$worst),
				array("Avg Score/Round",formatAverage($round_scores / $rounds)),
				array("Moons Shot",$moons),
				array("Moon Shooting %",formatPercent($moons / $moon_opts)),
				array("Avg Hearts Score",formatAverage($game_stats[0]['total'] / $game_stats[0]['attempts'])),
				array("Avg Uppers Score",formatAverage($game_stats[1]['total'] / $game_stats[1]['attempts'])),
				array("Avg Each Score",formatAverage($game_stats[2]['total'] / $game_stats[2]['attempts'])),
				array("Last Trick %",formatPercent($game_stats[3]['total'] / ($game_stats[3]['attempts']*10))),
				array("Red King %",formatPercent($game_stats[4]['total'] / ($game_stats[4]['attempts']*10))),
				array("Baby Blue %",formatPercent($game_stats[5]['total'] / ($game_stats[5]['attempts']*10))),
				array("Avg Quartet Score",formatAverage($game_stats[6]['total'] / $game_stats[6]['attempts'])),
				array("Avg Laracos Score",formatAverage($game_stats[7]['total'] / $game_stats[7]['attempts'])),
				)
			);
		die(json_encode(array("msg" => "success", "info" => $info,"post" => $_POST)));
	}
	die(json_encode(array("msg" => "error", "post" => $_POST)));
}
elseif($_GET['action'] == "leaderboard") {
	$rs = mysql_query("SELECT * FROM games") or die(mysql_error());
	$players = array();
	while($game = mysql_fetch_assoc($rs)) {
		$scores = json_decode($game['scores']);
		$p1id = $game['player1'];
		$p2id = $game['player2'];
		$p3id = $game['player3'];
		$p4id = $game['player4'];
		if(!isset($players[$p1id])) {
			$players[$p1id] = array('totals'=>array(),'games'=>array(
					array(),array(),array(),array(),array(),array(),array(),array()
					));
		}
		if(!isset($players[$p2id])) {
			$players[$p2id] = array('totals'=>array(),'games'=>array(
					array(),array(),array(),array(),array(),array(),array(),array()
					));
		}
		if(!isset($players[$p3id])) {
			$players[$p3id] = array('totals'=>array(),'games'=>array(
					array(),array(),array(),array(),array(),array(),array(),array()
					));
		}
		if(!isset($players[$p4id])) {
			$players[$p4id] = array('totals'=>array(),'games'=>array(
					array(),array(),array(),array(),array(),array(),array(),array()
					));
		}
		$totals = array(0,0,0,0);
		foreach($scores as $round) {
			foreach($round as $game_id => $hand) {
				if($hand[0] != 0 || $hand[1] != 0 || $hand[2] != 0 || $hand[3] != 0) {
					$players[$p1id]['games'][$game_id]['score'] += $hand[0];
					$players[$p2id]['games'][$game_id]['score'] += $hand[1];
					$players[$p3id]['games'][$game_id]['score'] += $hand[2];
					$players[$p4id]['games'][$game_id]['score'] += $hand[3];
					$players[$p1id]['games'][$game_id]['attempts']++;
					$players[$p2id]['games'][$game_id]['attempts']++;
					$players[$p3id]['games'][$game_id]['attempts']++;
					$players[$p4id]['games'][$game_id]['attempts']++;
				}
				$totals[0] += $hand[0];
				$totals[1] += $hand[1];
				$totals[2] += $hand[2];
				$totals[3] += $hand[3];
			}
		}
		if($game['round'] == 3 && $game['hand'] == 7) {
				$players[$p1id]['totals'][] = $totals[0];
				$players[$p2id]['totals'][] = $totals[1];
				$players[$p3id]['totals'][] = $totals[2];
				$players[$p4id]['totals'][] = $totals[3];
		}
		
	}
	$rsP = mysql_query("SELECT * FROM players WHERE maturity > 1 ORDER BY elo DESC LIMIT 5");
	$leaders = array(array("None Ranked","","N/A"),array("None Ranked","","N/A"),array("None Ranked","","N/A"));
	$i = 0;
	while($p = mysql_fetch_assoc($rsP)) {
		$leaders[$i] = array($p['name'],$p['id'],round($p['elo']));
		$i++;
	}

	$answer = array("msg" => "success", "info" => array(
			0 => array("Best Score","","",999),
			1 => array("Worst Score","","",0),
			2 => array("Best Avg Score","","",999),
			3 => array("Best Hearts","","",10),
			4 => array("Best Uppers","","",10),
			5 => array("Best Each","","",10),
			6 => array("Best Last","","",100),
			7 => array("Unluckiest Last","","",0),
			8 => array("Best Red King","","",100),
			9 => array("Unluckiest Red King","","",0),
			10 => array("Best Baby Blue","","",100),
			11 => array("Unluckiest Baby Blue","","",0),
			12 => array("Best Quartet","","",10),
			13 => array("Best Laracos","","",10)
			),
			"leaders" => $leaders
	);
	foreach($players as $p_id => $stats) {
		if($stats['games'][0]['attempts'] < 9 || count($stats['totals']) < 1) {
			unset($players[$p_id]);
		}
	}
	$names = $answer['info'];
	foreach($players as $p_id => $stats) {
		$name = getPlayerName($p_id);
		$best = min($stats['totals']);
		if($answer['info'][0][3] > $best) {
			$answer['info'][0] = array("",$name,$p_id,$best);
		}
		$worst = max($stats['totals']);
		if($answer['info'][1][3] < $worst) {
			$answer['info'][1] = array("",$name,$p_id,$worst);
		}
		$avg = formatAverage(array_sum($stats['totals']) / count($stats['totals']));
		if($answer['info'][2][3] > $avg) {
			$answer['info'][2] = array("",$name,$p_id,$avg);
		}
		$hearts = round($stats['games'][0]['score'] / $stats['games'][0]['attempts'] * 100) / 100;
		if($answer['info'][3][3] > $hearts) {
			$answer['info'][3] = array("",$name,$p_id,$hearts);
		}
		$uppers = round($stats['games'][1]['score'] / $stats['games'][1]['attempts'] * 100) / 100;
		if($answer['info'][4][3] > $uppers) {
			$answer['info'][4] = array("",$name,$p_id,$uppers);
		}
		$each = round($stats['games'][2]['score'] / $stats['games'][2]['attempts'] * 100) / 100;
		if($answer['info'][5][3] > $each) {
			$answer['info'][5] = array("",$name,$p_id,$each);
		}
		$last = round($stats['games'][3]['score'] / $stats['games'][3]['attempts'] * 1000) / 100;
		if($answer['info'][6][3] > $last) {
			$answer['info'][6] = array("",$name,$p_id,$last);
		}
		if($answer['info'][7][3] < $last) {
			$answer['info'][7] = array("",$name,$p_id,$last);
		}
		$king = round($stats['games'][4]['score'] / $stats['games'][4]['attempts'] * 1000) / 100;
		if($answer['info'][8][3] > $king) {
			$answer['info'][8] = array("",$name,$p_id,$king);
		}
		if($answer['info'][9][3] < $king) {
			$answer['info'][9] = array("",$name,$p_id,$king);
		}
		$baby = round($stats['games'][5]['score'] / $stats['games'][5]['attempts'] * 1000) / 100;
		if($answer['info'][10][3] > $baby) {
			$answer['info'][10] = array("",$name,$p_id,$baby);
		}
		if($answer['info'][11][3] < $baby) {
			$answer['info'][11] = array("",$name,$p_id,$baby);
		}
		$quartet = round($stats['games'][6]['score'] / $stats['games'][6]['attempts'] * 100) / 100;
		if($answer['info'][12][3] > $quartet) {
			$answer['info'][12] = array("",$name,$p_id,$quartet);
		}
		$laracos = round($stats['games'][7]['score'] / $stats['games'][7]['attempts'] * 100) / 100;
		if($answer['info'][13][3] > $laracos) {
			$answer['info'][13] = array("",$name,$p_id,$laracos);
		}
		
	}
	foreach($answer['info'] as $i => $stat) {
		$answer['info'][$i][0] = $names[$i][0];
		if($i >=6 && $i < 12) {
			$answer['info'][$i][] = "%";
		}
		else {
			$answer['info'][$i][] = "";
		}
	}
	die(json_encode($answer));
}
else {
	die(json_encode(array("msg" => "error", "post" => $_POST)));
}
